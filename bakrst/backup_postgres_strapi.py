#!/usr/bin/env python3

# Backup Paths
Paths = {
  'Postgres': f"/var/lib/postgresql/orion",
  'Strapi'  : f"/opt/strapi/Asterope",
}

Commands = {
  'Postgres': [],
  'Strapi'  : [],
}

print(f"imports")
from subprocess import run
from datetime import datetime
from os import (mkdir, chdir)

Containers = [
  'Postgres',
  'Strapi'
]

Names = {
  'Postgres'  : 'asterope-postgres',
  'Strapi'    : 'asterope-strapi',
}

_now = datetime.now().isoformat() 

print(f"chdir(archives)")
chdir('archives')

print(f"mkdir({_now})")
mkdir(_now)

print(f"chdir({_now})")
chdir(_now)

for _ctr in Containers:
  
  print(f"mkdir({_ctr})")
  mkdir(_ctr)
  
  print(f"chdir({_ctr})")
  chdir(_ctr)
  
  print(f"docker cp -a {Names[_ctr]}:{Paths[_ctr]} .")
  run(f"docker cp -a {Names[_ctr]}:{Paths[_ctr]} .", shell=True)

  print(f"chdir(..)")
  chdir("..")

print(f"push to git")  
run(f"git add --all ; git commit -am {_now} ; git push origin master", shell=True)
