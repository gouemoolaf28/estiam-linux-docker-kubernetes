#!/usr/bin/env python3

print(f"imports")
from subprocess import run

Names = {
  'Postgres'  : 'asterope__vpostgres',
  'Strapi'    : 'asterope__vstrapi',
}

_now = datetime.now().isoformat() 

print(f"chdir(archives)")
chdir('archives')

print(f"mkdir({_now})")
mkdir(_now)

print(f"chdir({_now})")
chdir(_now)

for _ctr in Containers:
  
  print(f"mkdir({_ctr})")
  mkdir(_ctr)
  
  print(f"chdir({_ctr})")
  chdir(_ctr)
  
  print(f"docker cp -a {Names[_ctr]}:{Paths[_ctr]} .")
  run(f"docker cp -a {Names[_ctr]}:{Paths[_ctr]} .", shell=True)
  
  # if _ctr == 'Postgres':
  #   print(f"docker exec -it {Names[_ctr]} {Commands[_ctr][0]}")
  #   run(f"docker exec -it {Names[_ctr]} {Commands[_ctr][0]}", shell=True)

  print(f"chdir(..)")
  chdir("..")

print(f"push to git")  
run(f"git add --all ; git commit -am {_now} ; git push origin master", shell=True)
